package com.deif.primeraactividad

import android.media.Rating
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*

var rb1:RadioButton?=null
var rb2:RadioButton?=null
var rb3:RadioButton?=null
var rb4:RadioButton?=null
var rb5:RadioButton?=null
var rb6:RadioButton?=null
var rb7:RadioButton?=null
var rb8:RadioButton?=null
var rb9:RadioButton?=null
var rb10:RadioButton?=null
var rb11:RadioButton?=null


class MainActivity : AppCompatActivity(), RadioGroup.OnCheckedChangeListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //botones del inicio
        var rg:LinearLayout = findViewById(R.id.scroll1)
        var btn1 = findViewById<View>(R.id.button1)
        var btn2 = findViewById<View>(R.id.button2)
        var btn3 = findViewById<View>(R.id.button3)
        var btn4 = findViewById<View>(R.id.button4)
        var btn5 = findViewById<View>(R.id.button5)
        var btn6 = findViewById<View>(R.id.button6)
        var btn7 = findViewById<View>(R.id.button7)

        btn1.setOnClickListener { botones1() }
        btn2.setOnClickListener { botones2() }
        btn3.setOnClickListener { botones3() }
        btn4.setOnClickListener { botones4() }
        btn5.setOnClickListener { botones5() }
        btn6.setOnClickListener { botones6() }
        btn7.setOnClickListener { botones7() }
        //rating bar


        val rtb:RatingBar=findViewById(R.id.ratingBar2)
        rtb.setOnRatingBarChangeListener { ratingBar, fl, b -> msg("calificación $fl") }
        //radio group
        var rq:RadioGroup = findViewById(R.id.rg)
        rb1=findViewById(R.id.radioButton1)
        rb2=findViewById(R.id.radioButton2)
        rb3=findViewById(R.id.radioButton3)
        rb4=findViewById(R.id.radioButton4)
        rb5=findViewById(R.id.radioButton5)
        rb6=findViewById(R.id.radioButton6)
        rb7=findViewById(R.id.radioButton7)
        rb8=findViewById(R.id.radioButton8)
        rb9=findViewById(R.id.radioButton9)
        rb10=findViewById(R.id.radioButton10)
        rb11=findViewById(R.id.radioButton11)

        rq?.setOnCheckedChangeListener(this)
        //check box
        val box1:CheckBox=findViewById(R.id.checkBox2)
        val box2:CheckBox=findViewById(R.id.checkBox)
        box1.setOnClickListener {
            if(box1.isChecked)
            {
                msg2("CheckBox1")
            }
        }
        box2.setOnClickListener {
            if (box2.isChecked)
            {
                msg2("CheckBox2")
            }
        }


    }
    override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {

        when(p1){
            rb1?.id ->msg2("1")
            rb2?.id ->msg2("2")
            rb3?.id ->msg2("3")
            rb4?.id ->msg2("4")
            rb5?.id ->msg2("5")
            rb6?.id ->msg2("6")
            rb7?.id ->msg2("7")
            rb8?.id ->msg2("8")
            rb9?.id ->msg2("9")
            rb10?.id ->msg2("10")
            rb11?.id ->msg2("11")
        }
    }
    fun msg2 (number:String){
        val tv:TextView = findViewById(R.id.textView2)
        tv.setText("radio $number")
        var msg=Toast.makeText(this, "Radio $number", Toast.LENGTH_SHORT)
        msg.setGravity(Gravity.TOP,0,0)
        msg.show()
    }

    fun msg(number: String){
        var tv:TextView =findViewById(R.id.textView2)
        tv.setText("$number estrellas")
        var msg=Toast.makeText(this, "Radio $number", Toast.LENGTH_SHORT)
        msg.setGravity(Gravity.TOP,0,0)
        msg.show()
    }

    fun botones1(){
    var indicativo:TextView =findViewById(R.id.textView2)

        indicativo.text="boton 1"
    }

    fun botones2(){
        var indicativo:TextView =findViewById(R.id.textView2)

        indicativo.text="boton 2"
    }

    fun botones3(){
        var indicativo:TextView =findViewById(R.id.textView2)

        indicativo.text="boton 3"
    }

    fun botones4(){
        var indicativo:TextView =findViewById(R.id.textView2)

        indicativo.text="boton 4"
    }

    fun botones5(){
        var indicativo:TextView =findViewById(R.id.textView2)

        indicativo.text="boton 5"
    }

    fun botones6(){
        var indicativo:TextView =findViewById(R.id.textView2)

        indicativo.text="boton 6"
    }
    fun botones7(){
        var indicativo:TextView =findViewById(R.id.textView2)

        indicativo.text="boton Si"
    }
}
